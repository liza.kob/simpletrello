package com.simpletrello.goal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.simpletrello.task.Task;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "goals")
@Where(clause = "deleted = false")
public class Goal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "deleted", nullable = false)
    private boolean delete;

    @Column(name = "position", nullable = false)
    private int position;

    @JsonIgnoreProperties("goal")
    @OneToMany(mappedBy = "goal")
    private List<Task> tasks = new ArrayList<>();
}
