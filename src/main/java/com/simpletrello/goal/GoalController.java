package com.simpletrello.goal;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/goals")
public class GoalController {

    private final GoalService goalService;

    @GetMapping
    public List<Goal> getAllGoals() {
        return goalService.getAllGoals();
    }

    @PostMapping
    public void saveGoal(@RequestBody Goal goal) {
        goalService.saveNewGoal(goal);
    }

    @PutMapping("/{id}")
    public void changeGoalPosition(@PathVariable int id, @RequestBody Goal goal) {
        goalService.updateGoal(id, goal);
    }

    @PutMapping("/{id}/change_position")
    public void changeGoalPosition(@PathVariable int id, int position) {
        goalService.changeGoalPosition(id, position);
    }

    @DeleteMapping("/{id}")
    public void deleteGoal(@PathVariable int id) {
        goalService.deleteGoal(id);
    }
}
