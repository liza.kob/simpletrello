package com.simpletrello.goal;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

@AllArgsConstructor
@Service
public class GoalService {
    private final GoalRepository goalRepository;

    public List<Goal> getAllGoals() {
        List<Goal> goals = goalRepository.findAll();
        return sortGoalsByPosition(goals);
    }

    private List<Goal> sortGoalsByPosition(List<Goal> goals) {
        goals.sort(Comparator.comparing(Goal::getPosition));
        return goals;
    }

    public void changeGoalPosition(int goalId, int position) {
        Goal currGoal = goalRepository.findById(goalId).orElseThrow(() ->
                new NoSuchElementException("Can't find goal with id" + goalId));

        List<Goal> goals = getAllGoals();
        for (Goal goal : goals) {
            if ((goal.getPosition() >= position) && (goal.getPosition() < currGoal.getPosition())) {
                goal.setPosition(goal.getPosition() + 1);
                goalRepository.save(goal);
            }
        }
        currGoal.setPosition(position);
        goalRepository.save(currGoal);
    }

    public void saveNewGoal(Goal goal) {
        goal.setDelete(false);
        goalRepository.save(goal);
    }

    public void deleteGoal(int goalId) {
        Goal goal = goalRepository.findById(goalId).orElseThrow(() ->
                new NoSuchElementException("Can't find goal with id" + goalId));
        goal.setDelete(true);
        goalRepository.save(goal);
    }

    public void updateGoal(int goalId, Goal updateGoal) {
        Goal goal = goalRepository.findById(goalId).orElseThrow(()
                -> new NoSuchElementException("Can't find goal with id" + goalId));
        goal.setName(updateGoal.getName());
        goalRepository.save(goal);
    }
}
