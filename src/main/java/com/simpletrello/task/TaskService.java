package com.simpletrello.task;

import com.simpletrello.goal.Goal;
import com.simpletrello.goal.GoalRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

@AllArgsConstructor
@Service
public class TaskService {

    private final TaskRepository taskRepository;
    private final GoalRepository goalRepository;

    public List<Task> getAllTasks(int goalId) {
        List<Task> tasks = taskRepository.findAllByGoalId(goalId);
        return sortGoalsByDate(tasks);
    }

    public Task getById(int taskId) {
        return taskRepository.findById(taskId).orElseThrow(()
                -> new NoSuchElementException("Can't find task with id" + taskId));
    }

    private List<Task> sortGoalsByDate(List<Task> tasks) {
        tasks.sort(Comparator.comparing(Task::getCreatedDate));
        return tasks;
    }

    public void saveNewTask(Task task, int goalId) {
        task.setCreatedDate(LocalDate.now());
        task.setDone(false);
        task.setDelete(false);
        Goal goal = goalRepository.findById(goalId).orElseThrow(()
                -> new NoSuchElementException("Can't find goal with id" + goalId));
        task.setGoal(goal);

        taskRepository.save(task);
    }

    public void changeGoal(int taskId, Goal changeGoal) {
        Task task = taskRepository.findById(taskId).orElseThrow(()
                -> new NoSuchElementException("Can't find task with id" + taskId));
        Goal goal = goalRepository.findById(changeGoal.getId()).orElseThrow(()
                -> new NoSuchElementException("Can't find goal with id" + changeGoal.getId()));
        task.setGoal(goal);
        taskRepository.save(task);
    }

    public void updateTask(int taskId, Task updateTask) {
        Task task = taskRepository.findById(taskId).orElseThrow(()
                -> new NoSuchElementException("Can't find task with id" + taskId));
        task.setName(updateTask.getName());
        task.setDescription(updateTask.getDescription());
        task.setDone(updateTask.isDone());
        taskRepository.save(task);
    }

    public void deleteTask(int taskId) {
        Task task = taskRepository.findById(taskId).orElseThrow(() ->
                new NoSuchElementException("Can't find task with id" + taskId));
        task.setDelete(true);
        taskRepository.save(task);
    }
}
