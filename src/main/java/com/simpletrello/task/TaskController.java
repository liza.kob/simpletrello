package com.simpletrello.task;

import com.simpletrello.goal.Goal;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/tasks")
public class TaskController {

    private final TaskService taskService;

    @GetMapping
    public List<Task> getAllTasksForGoal(int goalId) {
        return taskService.getAllTasks(goalId);
    }

    @GetMapping
    public Task getById(int id) {
        return taskService.getById(id);
    }

    @PostMapping
    public void saveTasks(@RequestBody Task task, int goalId) {
        taskService.saveNewTask(task, goalId);
    }

    @PutMapping("/{id}")
    public void updateTasks(@PathVariable int id, @RequestBody Task task) {
        taskService.updateTask(id, task);
    }

    @PutMapping("/{id}/change_goal")
    public void changeGoal(@PathVariable int id, @RequestBody Goal changeGoal) {
        taskService.changeGoal(id, changeGoal);
    }

    @DeleteMapping("/{id}")
    public void deleteTasks(@PathVariable int id) {
        taskService.deleteTask(id);
    }
}
