package com.simpletrello.task;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.simpletrello.goal.Goal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tasks")
@Where(clause = "deleted = false")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "createddate", nullable = false)
    private LocalDate createdDate;

    @Column(name = "done", nullable = false)
    private boolean done;

    @Column(name = "deleted", nullable = false)
    private boolean delete;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "goal_id")
    private Goal goal;
}
